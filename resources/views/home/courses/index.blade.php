@extends('layouts.app', ['class' => 'bg-default'])
@section('content')


	
 @if(!empty($slider) and !$isSearch)
 <section id="Home">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators" >
            @foreach($slider as $i => $e)
            <li data-target="#carouselExampleIndicators" data-slide-to="{{$i}}" class="@if($i == 0) active @endif"></li>
            @endforeach
          </ol>
          <div class="carousel-inner">
            @foreach($slider as $i => $e)

            <a href="{{ route('home.slider.detail', $e->id) }}" class="carousel-item @if($i == 0) active @endif">
              <img class="d-block w-100" src="{{ asset('images') }}/{{ $e->thumbnail }}" width="100%" style="object-fit: cover;">
              {{-- <div class="carousel-caption d-none d-md-block"> --}}
                {{-- <h2 class="text-white">{{$e->name}}</h2> --}}
                {{--  <p>{{Str::limit($e->content, 100, $end='.')}}</p>  --}}
              {{-- </div> --}}
            </a>

            @endforeach
          </div>
          <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </section
    @endif
    



<br>

<section id="Acara">


</section>

@if(!empty($category))

<br>

 <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-8 col-xs-8 ml-2">
        <div class="section-headline">
          <h2>ALL STORE</h2>          
        </div>
      </div>
    </div>  
    <div class="owl-carousel owl-theme">

 @foreach($category as $i => $e)
    



    <div class="item">
     <div class="desain">
          <div class="single-awesome-project">           
            <div class="desain-md-12 desain-sm-12 desain-xs-12 desain-lg-12" style=" max-height:200px; overflow:hidden;margin: 5px 5px 5px 5px;">
               <a class="text-black" href="{{ route('home.category.detail', $e->id) }}">
                <img src="{{ asset('images') }}/{{ $e->thumbnail }}">
                   </a>
            </div>
             
             
         </div>
        </div>
      </div>
      @endforeach
      </div>
  </div>
  </div>
<br>
<br>

@endif




@endsection
