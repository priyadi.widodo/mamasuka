@extends('layouts.app')

@section('content')
<div class="container mt-5" >
  <div class="row mt-3">
    <div class="col-md-12">
      <div class="card" style="margin-top:100px;">
        <div class="card-header">
          <div class="row align-items-center">
            <div class="col">
              <span><h5 class="text-uppercase ls-1 mb-1">Ikuti Mamasuka Festival</h5></span>
              <h2 class="text-uppercase">{{ $pageName }}</h2>
            </div>
          </div>
        </div>
        <div class="card-body" style="bg-color:gray;">
          <form method="POST" action="{{ route('home.transaction.store') }}" enctype="multipart/form-data" novalidate>
                
                <input type="text" class="form-control" name="id" value="{{ auth()->user()->user_id }}" hidden>
              
 
 
 
 
 
 
 
 <div class="row mb-3">
              <label for="name" class="col-md-4 col-form-label text-md-end">Account Name</label>

              <div class="col-md-6">
               
              <select class="form-control @error('category') is-invalid @enderror" name="category_id" id="category_id" >
                  <option style="font-weight: bold;" selected>Departemen Store</option>
                  @foreach ($category as $key => $value)
                  


                  <option value="{{ $value->id }}" @if($value->id == old('category_id')) selected @endif>{{ $value->name }}</option>
                  
                 @endforeach
                </select>

                @error('category_id')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror
              
              </div>
            </div>




<div class="row mb-3">
              <label for="name" class="col-md-4 col-form-label text-md-end">Location</label>

              <div class="col-md-6">
               
              <select class="form-control @error('toko_id') is-invalid @enderror" name="toko_id" id="toko_id" >
                  <option style="font-weight: bold;" selected>Pilih Lokasi</option>
    
         @foreach ($tokos as $key => $value)
       
                  
                  <option value="{{ $value->id }}" @if($value->id == old('toko_id')) selected @endif>{{ $value->name_toko }}</option>
                  @endforeach
              </select>
         
         






                @error('toko_id')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror
              
              </div>
            </div>



            <div class="row mb-3">
              <label for="name" class="col-md-4 col-form-label text-md-end">Product Amount</label>

              <div class="col-md-6">
                <input type="text" class="form-control" name="product_amount" value="">
              @error('product_amount')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror
              
              
              </div>
            </div>


            <div class="row mb-3">
              <label for="name" class="col-md-4 col-form-label text-md-end">Unique Code</label>

              <div class="col-md-6">
                <input type="text" class="form-control" value="" name="unique_code">
               @error('unique_code')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror
              
              
              
              </div>
            </div>

           

            <div class="row mb-3">
              <label for="name" class="col-md-4 col-form-label text-md-end">Kode Kupon</label>

              <div class="col-md-6">
                <input type="text" class="form-control " value="" name="coupon_code">
                 @error('coupon_code')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
            </div>

            <div class="row mb-3">
              <label class="col-md-4 col-form-label text-md-end">Bukti Pembayaran</label>

              <div class="col-md-6">
                <input type="file" class="form-control @error('proof') is-invalid @enderror" name="proof" required>

                @error('proof')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
            </div>

            <div class="row mb-0">
              <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-danger col-3">
                  Simpan
                </button>
              </div>
            </div>
          </form>

        </div>
      </div>
    </div>
  </div>
</div>
<br>
@endsection