@extends('layouts.appadmin')

@section('content')
<div class="container mt-5">
  <div class="row mt-3">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <div class="row align-items-center">
            <div class="col">
              <h6 class="text-uppercase ls-1 mb-1">Tambah Data</h6>
              <h2 class="text-uppercase">{{ $pageName }}</h2>
            </div>
          </div>
        </div>
        <div class="card-body">

          <form method="POST" action="{{ route('admin.toko.store') }}" enctype="multipart/form-data" novalidate>
            @csrf


<div class="row mb-3">
             

              
                            <div class="col-md">
                                  <div class="form-group">
                                  <div class="input-group">
                                     <div class="input-group-prepend">
                                     <span class="input-group-text"><i class="ni ni-pin-3"></i></span>
                                     </div> 
              <select class="form-control @error('category_id') is-invalid @enderror" name="category_id" id="category_id" >
                  <option style="font-weight: bold;" selected>Pilih Lokasi</option>
                  @foreach ($category as $key => $value)
                  <option value="{{ $value->id }}" @if($value->id == old('category_id')) selected @endif>{{ $value->name }}</option>
                  @endforeach
                </select>

                @error('category_id')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror
              
              </div>
            </div>




                    <div class="row mb-1">
                            <div class="col-md">
                                  <div class="form-group">
                                  <div class="input-group">
                                     <div class="input-group-prepend">
                                     <span class="input-group-text"><i class="ni ni-shop"></i></span>
                                     </div>
                                      <input id="name_toko" type="text" class="form-control @error('name_toko') is-invalid @enderror" name="name_toko" value="{{ old('name_toko') }}" placeholder="{{ __('Store Name') }}" required autocomplete="name_toko" autofocus>

                                @error('name_toko')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                     </div>
                                    </div>
                             </div>
                        </div>



                        
                        
                     <div class="row mb-1">
                            <div class="col-md">
                                  <div class="form-group">
                                  <div class="input-group">
                                     <div class="input-group-prepend">
                                     <span class="input-group-text"><i class="ni ni-pin-3"></i></span>
                                     </div>
                                       <input id="kota" type="kota" class="form-control @error('kota') is-invalid @enderror" name="kota" value="{{ old('kota') }}" placeholder="{{ __('Kota') }}" required >

                                @error('kota')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                     </div>
                                    </div>
                             </div>
                        </div>
                    
                                     <div class="row mb-1">
                            <div class="col-md">
                                  <div class="form-group">
                                  <div class="input-group">
                                     <div class="input-group-prepend">
                                     <span class="input-group-text"><i class="ni ni-pin-3"></i></span>
                                     </div>
                                       <input id="alamat" type="text" class="form-control @error('alamat') is-invalid @enderror" name="alamat" placeholder="Alamat" required >
                                       @error('alamat')
                                      <span class="invalid-feedback" role="alert">
                                       <strong>{{ $message }}</strong>
                                    </span>
                                     @enderror

                                </div>
                            </div>
                             </div>
                        </div>



                        <div class="row mb-1">
                            <div class="col-md-6 offset-md-0">
                               <button type="submit" class="btn btn-info">
                               Simpan
                          </button>
                        </div>
            </div>
          </form>

        </div>
      </div>
    </div>
  </div>
</div>
@endsection