@extends('layouts.appadmin')

@section('content')
<div class="container mt-5">
  @if(session('success'))
  <div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
    {{session('success')}}
  </div>
  @endif
  <div class="row mt-3">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <div class="row align-items-center">
            <div class="col">
              <h6 class="text-uppercase ls-1 mb-1">Master Data</h6>
              <h2 class="text-uppercase">{{ $pageName }}</h2>
            </div>
            <div class=" col">
              <ul class="nav nav-pills justify-content-end">
                <li class="nav-item">
                  <a class="btn btn-info btn-md" href="{{ route('admin.toko.create') }}">Tambah</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="card-body">

          <div class="table-responsive">
            <table class="table align-items-center">
              <thead class="thead-light">
                <tr>
                  <th scope="col" class="text-center" style="width: 50px;">#</th>
                  <th scope="col" class="text-center" style="width: 100px;">#</th>
                  <th scope="col" class="text-center">Kategori</th>
                  <th scope="col" class="text-center">Store Name</th>
                  <th scope="col" class="text-center">Kota</th>
                   <th scope="col" class="text-center">Alamat</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($data as $key => $value)
                <tr>
                  <td class="text-center">{{$loop->iteration}}</td>
                  <td class="text-center">

    


                      <form action="{{ route('admin.toko.destroy',$value->id) }}" method="post" class="d-inline">

                      <a class="btn btn-primary btn-sm" href="{{ route('admin.toko.edit',$value->id) }}">Edit</a>



                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are You Sure Delete  ')">
                                        Hapus </button>
                                </form>

                  </td>
                  <td>{{ $value->name }}
                  </td>
                  <td>
                  {{ $value->name_toko }}</td>
                  
                <td>
                  {{ $value->kota }}</td>
                   <td>
                  {{ $value->alamat }}</td>
                  
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
@endsection