@extends('layouts.appadmin')

@section('content')
<div class="container mt-5">
  <div class="row mt-3">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <div class="row align-items-center">
            <div class="col">
              <h6 class="text-uppercase ls-1 mb-1">Edit Data</h6>
              <h2 class="text-uppercase">{{ $pageName }}</h2>
            </div>
          </div>
        </div>
        <div class="card-body">

          <form method="POST" action="{{ route('admin.toko.update', $toko->id) }}" enctype="multipart/form-data" novalidate>
            @csrf
            @method('PUT')

<div class="row mb-3">
              <label for="name" class="col-md-4 col-form-label text-md-end">Pilih Lokasi</label>

              <div class="col-md-6">

<select class="form-control @error('category_id') is-invalid @enderror" name="category_id" id="category_id" >
                  <option style="font-weight: bold;" selected>Pilih Lokasi</option>
                  @foreach ($category as $key => $value)
                  <option value="{{ $value->id }}" @if($value->id == old('category_id')) selected @endif>{{ $value->name }}</option>
                  @endforeach
                </select>

               











 </div>
            </div>

            <div class="row mb-3">
              <label for="name" class="col-md-4 col-form-label text-md-end">Nama Toko</label>

              <div class="col-md-6">
                <input id="name_toko" type="text" class="form-control @error('name_toko') is-invalid @enderror" name="name_toko" value="{{ old('name_toko') ?? $toko->name_toko }}" required autocomplete="name_toko" placeholder="Nama Kategori">

                @error('name_toko')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
            </div>

          


            <div class="row mb-3">
              <label for="name" class="col-md-4 col-form-label text-md-end">Kota</label>

              <div class="col-md-6">
                <input id="kota" type="text" class="form-control @error('kota') is-invalid @enderror" name="kota" value="{{ old('kota') ?? $toko->kota }}" required autocomplete="kota" placeholder="Kota">

                @error('kota')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
            </div>


            <div class="row mb-3">
              <label for="name" class="col-md-4 col-form-label text-md-end">Alamat</label>

              <div class="col-md-6">
                <input id="alamat" type="text" class="form-control @error('alamat') is-invalid @enderror" name="alamat" value="{{ old('alamat') ?? $toko->alamat }}" required autocomplete="alamat" placeholder="Nama Kategori">

                @error('alamat')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
            </div>





            <div class="row mb-0">
              <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-info">
                  Simpan
                </button>
              </div>
            </div>
          </form>

        </div>
      </div>
    </div>
  </div>
</div>
@endsection