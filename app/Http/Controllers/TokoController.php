<?php

namespace App\Http\Controllers;
use App\Models\Toko;
use App\Models\Category;
use Hash;

use Illuminate\Http\Request;

class TokoController extends Controller
{
    public function index()
    {

        


        $data = Toko::join('category', 'category.id', '=', 'tokos.category_id')
        ->get(['tokos.*','category.name']);

        $pageName = 'Store Festival';
        return view('admin.toko.index', compact('data', 'pageName'));
    }


    public function create()
    {
        $pageName = 'Store Festival';
        $category= Category::all();
        return view('admin.toko.create', compact('pageName','category'));
    }



    public function store(Request $request)
    {
        $request->validate([
            'category_id' => 'required|not_in:"Pilih Lokasi"',
            'name_toko' => 'required',
            'alamat' => 'required',
            'kota' => 'required',
        ], config('global.validator'));

        Toko::create([
            'category_id' => $request->category_id,
            'name_toko' => $request->name_toko,
            'alamat' => $request->alamat,
            'kota' => $request->kota,
        ]);

        return redirect()->route('admin.toko')
            ->with('success', 'Berhasil menambah Toko.');
    }


    public function edit(Toko $toko)
    {   
        $category = Toko::join('category', 'category.id', '=', 'tokos.category_id')
        ->get(['tokos.*','category.name']);


        $pageName = 'Store Festival';
        return view('admin.toko.edit', compact('category','toko', 'pageName'));
    }




    public function update(Request $request, Toko $toko)
    {  
        $rules=[
            'category_id' => 'not_in:"Pilih Lokasi"',
            'name_toko' => 'required',
            'alamat' => 'required',
            'kota' => 'required',
            
        ];

        $validateData = $request->validate($rules); //validasi
  
            
        $toko->update($validateData,[
            'category_id' => $request->category_id,
            'name_toko' => $request->name_toko,
            'alamat' => $request->alamat,
            'kota' => $request->kota,
			 ]);

        return redirect()->route('admin.toko')
            ->with('success', 'Berhasil diedit.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Toko $toko)
    {
        
        Toko::where('id', $toko->id)->delete();

        $toko->delete();

        return redirect()->route('admin.toko')
            ->with('success', 'Berhasil dihapus.');
    
    }



    
}
