<?php

namespace App\Http\Controllers;

use App\Models\Category;

use App\Models\User;


use App\Models\Slider;
use App\Models\Toko;


use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
       
        $pageName = 'Kursus Terbaru';
        $isSearch = false;
        if (request('search')) {
            $data->where('name', 'Like', '%' . request('search') . '%');
            $pageName = request('search');
            $isSearch = true;
        }

      
        $category = Category::orderby('name', 'asc')->get();

        
        $slider = Slider::orderby('created_at', 'desc')->limit(5)->get();


        return view('home.courses.index', compact( 'pageName', 'isSearch', 'category','slider'));
    }

    public function categoryDetail($id)
    {
        

        return view('home.courses.category', compact('data', 'pageName', 'category'));
    }

    public function mine()
    {
        
        $data = User::all();
        $category= Category::all();
        $tokos= Toko::all();
        
        
      
        $pageName = 'Akun Saya';
       
        return view('home.transaction.create', compact('data', 'pageName','category','tokos'));
    }

    



   


    public function adminHome()
    {
        return view('admin.home');
    }



    public function slider()
    {
        $data = Slider::orderBy('created_at', 'desc')->get();
        return view('home.slider.index', compact('data'));
    }

    public function sliderDetail($id)
    {
        $data = Slider::find($id);
        return view('home.slider.detail', compact('data'));
    }






   
}
