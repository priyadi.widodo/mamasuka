<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
class Toko extends Model
{ 
    use Uuids, HasFactory;

    protected $table = 'tokos';

    protected $fillable = [
        'category_id',
        
        'name_toko',
        'alamat',
        'kota',
        
    ];
}
